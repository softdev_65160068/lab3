/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lap3;

/**
 *
 * @author USER
 */
public class Lap3 {
     public static boolean isWin(char[][] table,char currentplayer){
        for(int row = 0;row<3;row++){
            if(table[row][0] == currentplayer && table[row][1]== currentplayer && table[row][2]== currentplayer){
                return true;
            }
        }
        for(int col = 0; col<3;col++){
            if(table[0][col] == currentplayer && table[1][col]== currentplayer && table[2][col]== currentplayer){
                    return true;
            }
        }
        if(table[0][0]== currentplayer && table[1][1] == currentplayer && table[2][2] == currentplayer){
            return true;
        }
        if(table[0][2]== currentplayer && table[1][1] == currentplayer && table[2][0] == currentplayer){
            return true;
        }
        return false;
        
    }
      public static boolean isDraw(char[][] table,char currentplayer){
        for(int i =0; i<3;i++){
            for(int j = 0; j<3; j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
 }
}

