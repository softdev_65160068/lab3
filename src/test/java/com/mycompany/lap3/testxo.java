package com.mycompany.lap3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author USER
 */
public class testxo {
    
    public testxo() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow_0_0_output_true(){
        char [][] table = {{'o','o','o'},{'-','-','-'},{'-','-','-'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow_1_1_output_true(){
        char [][] table = {{'-','-','-'},{'o','o','o'},{'-','-','-'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinRow_2_2_output_true(){
        char [][] table = {{'-','-','-'},{'-','-','-'},{'o','o','o'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWincol_0_0_output_true(){
        char [][] table = {{'o','-','-'},{'o','-','-'},{'o','-','-'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
      @Test
    public void testCheckWincol_1_1_output_true(){
        char [][] table = {{'-','o','-'},{'-','o','-'},{'-','o','-'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
       @Test
    public void testCheckWincol_2_2_output_true(){
        char [][] table = {{'-','-','o'},{'-','-','o'},{'-','-','o'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(true,result);
    }
        @Test
    public void testCheckdraw_0_0_output_true(){
        char [][] table = {{'x','o','o'},{'o','x','o'},{'x','o','x'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(false,result);
    }
      @Test
      public void testCheckdraw_1_1_output_true(){
        char [][] table = {{'x','o','x'},{'o','x','x'},{'x','o','o'}};
        char currentplayer = 'o';
        boolean result = Lap3.isWin(table,currentplayer);
        assertEquals(false,result);
    }
    
    
}

